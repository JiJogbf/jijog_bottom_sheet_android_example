package jijog.bottomsheet

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import jijog.bottomsheet.ui.theme.BottomSheetDemoTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BottomSheetDemoTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    BottomSheetDemo()
                }
            }
        }
    }
}


@Composable
fun BottomSheetControl(initialState: Boolean, setState: (Boolean)->Unit, origin: @Composable ()->Unit){
    val offsetAnimation: Dp by animateDpAsState(
        if(initialState) 0.dp else 500.dp
    )

    Box {
        Column(modifier = Modifier.background(color = if(initialState) Color(0x55000000) else Color.Transparent).fillMaxSize()){}
        origin()
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Spacer(modifier = Modifier.weight(1f))
            Column(
                modifier = Modifier
                    .offset(0.dp, offsetAnimation)
                    .clickable(onClick = {setState(!initialState)})
                    .fillMaxWidth()
                    .background(
                        Color.White,
                        RoundedCornerShape(topStart = 32.dp, topEnd = 32.dp)
                    )
            ) {
                Column(
                    modifier = Modifier
                        .background(
                            color = Color.LightGray,
                            shape = RoundedCornerShape(topStart = 32.dp, topEnd = 32.dp)
                        )
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = "Energy",
                        modifier = Modifier.padding(8.dp)
                    )
                    Text(
                        text = "Datetime here",
                        modifier = Modifier.padding(8.dp)
                    )
                }
                Text(
                    text = "Linear tarif here",
                    modifier = Modifier.padding(8.dp)
                )
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceAround){
                    Column{
                        Text(
                            text = "New tarif value",
                            modifier = Modifier.padding(8.dp)
                        )
                        TextField(modifier = Modifier.width(128.dp), value = "", onValueChange = {})
                    }
                    Column{
                        Text(
                            text = "New tarif value",
                            modifier = Modifier.padding(8.dp)
                        )
                        TextField(modifier = Modifier.width(128.dp), value = "", onValueChange = {})
                    }
                }
                Text(
                    text = "Night tarif here",
                    modifier = Modifier.padding(8.dp)
                )
                Row(modifier = Modifier.fillMaxWidth().padding(bottom = 8.dp), horizontalArrangement = Arrangement.SpaceAround){
                    Column{
                        Text(
                            text = "New tarif value",
                            modifier = Modifier.padding(8.dp)
                        )
                        TextField(modifier = Modifier.width(128.dp), value = "", onValueChange = {})
                    }
                    Column{
                        Text(
                            text = "New tarif value",
                            modifier = Modifier.padding(8.dp)
                        )
                        TextField(modifier = Modifier.width(128.dp), value = "", onValueChange = {})
                    }
                }
                Button(modifier = Modifier.fillMaxWidth().padding(8.dp), onClick = {

                }){
                    Text("Commit")
                }
            }
        }
    }
}

@Composable
fun BottomSheetDemo() {
    var (state, setState) = remember { mutableStateOf(false)}
    BottomSheetControl(state, setState){
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
        ) {
            item{
                Button(modifier = Modifier.padding(8.dp), onClick = {setState(!state)}){
                    Text("Expand")
                }
            }
            repeat(30) {
                item {
                    Text("This is text with some label")
                }
            }
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun DefaultPreview() {
    BottomSheetDemoTheme {
        BottomSheetDemo()
    }
}